////
////  APIProvider.swift
////  RAWGame
////
////  Created by Ahmad Nur Alifulloh on 25/07/22.
////
//
//import Foundation
//import Alamofire
//
//private let baseUrl = "https://api.rawg.io/api"
//
//private var apiKey: String {
//    get {
//        guard let filePath = Bundle.main.path(forResource: "Info", ofType: "plist") else {
//            fatalError("Couldn't find file 'Info.plist'.")
//        }
//        let plist = NSDictionary(contentsOfFile: filePath)
//        guard let value = plist?.object(forKey: "API_KEY") as? String else {
//            fatalError("Couldn't find key 'API_KEY' in 'Info.plist'.")
//        }
//        return value
//    }
//}
//protocol ApiProviderProtocol{
//    func get(url: String, parameter: [String: Any], completion: @escaping (Result<Data, Error>) -> Void)
//}
//
//class ApiProvider: ApiProviderProtocol {
//    
//    func get(url: String, parameter: [String: Any], completion: @escaping (Result<Data, Error>) -> Void) {
//        AF.request(
//            url,
//            method: .get,
//            parameters: parameter,
//            encoding: URLEncoding.default
//        ).responseData { response in
//            switch response.result {
//            case .success(let data):
//                completion(.success(data))
//            case .failure(let error):
//                completion(.failure(error))
//            }
//        }
//    }
//}
