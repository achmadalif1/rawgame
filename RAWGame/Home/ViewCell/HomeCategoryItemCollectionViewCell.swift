//
//  HomeCategoryItemCollectionViewCell.swift
//  RAWGame
//
//  Created by Ahmad Nur Alifulloh on 25/07/22.
//

import UIKit

class HomeCategoryItemCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var contentViewCell: UIView!
    @IBOutlet weak var categoryItemLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        setUpView()
    }
    func setUpView(){

        self.contentViewCell.layer.cornerRadius = 12
        self.contentViewCell.layer.borderColor = UIColor.gray.cgColor
        self.contentViewCell.layer.shadowOffset = CGSize(width: 10, height: 10)
        self.contentViewCell.layer.shadowRadius = 3
        self.contentViewCell.layer.shadowOpacity = 0.1
        self.contentViewCell.layer.shadowOffset = .zero
        self.contentViewCell.layer.shadowColor = UIColor.black.cgColor
        self.clipsToBounds = true
        
    }

}
