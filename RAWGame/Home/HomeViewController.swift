//
//  HomeViewController.swift
//  RAWGame
//
//  Created by Ahmad Nur Alifulloh on 20/07/22.
//

import UIKit

class HomeViewController: UIViewController {
    @IBOutlet weak var genresCategoryButton: UIButton!
    
    @IBOutlet weak var gameItemTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        setUpGameItemTableView()
        genresCategoryButton.addTarget(self, action: #selector(showCategoryItemListPopUp), for: .touchUpInside)
    }
    
    @objc func showCategoryItemListPopUp() {
        let slideVC = HomeCategoryItemViewController()
        slideVC.modalPresentationStyle = .custom
        slideVC.transitioningDelegate = self
        self.present(slideVC, animated: true, completion: nil)
    }
}
extension HomeViewController: UIViewControllerTransitioningDelegate {
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        PresentationController(presentedViewController: presented, presenting: presenting)
    }
}
extension HomeViewController: UITableViewDataSource, UITableViewDelegate{
    private func setUpGameItemTableView(){
        let nibName = UINib(nibName: "HomeGameItemTableViewCell", bundle: nil)
        gameItemTableView.register(nibName, forCellReuseIdentifier: "HomeGameItemTableViewCell")
        gameItemTableView.delegate = self
        gameItemTableView.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeGameItemTableViewCell", for: indexPath) as! HomeGameItemTableViewCell
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = DetailGameViewController()
        self.navigationController?.present(vc, animated: true)
    }
    
}
