//
//  GamesModel.swift
//  RAWGame
//
//  Created by Ahmad Nur Alifulloh on 28/07/22.
//

import Foundation
// MARK: - Games
struct GamesResponses: Codable {
    let count: Int
    let next, previous: String
    let results: [GamesResponsesResult]
}

// MARK: - Result
struct GamesResponsesResult: Codable {
    let id: Int
    let slug, name, released: String
    let tba: Bool
    let backgroundImage: String
    let rating, ratingTop: Int
    let ratings: String
    let ratingsCount: Int
    let reviewsTextCount: String
    let added: Int
    let addedByStatus: String
    let metacritic, playtime, suggestionsCount: Int
    let updated: Date
    let esrbRating: String
    let platforms: [String]

    enum CodingKeys: String, CodingKey {
        case id, slug, name, released, tba
        case backgroundImage = "background_image"
        case rating
        case ratingTop = "rating_top"
        case ratings
        case ratingsCount = "ratings_count"
        case reviewsTextCount = "reviews_text_count"
        case added
        case addedByStatus = "added_by_status"
        case metacritic, playtime
        case suggestionsCount = "suggestions_count"
        case updated
        case esrbRating = "esrb_rating"
        case platforms
    }
}
