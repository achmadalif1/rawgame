////
////  HomeViewModel.swift
////  RAWGame
////
////  Created by Ahmad Nur Alifulloh on 28/07/22.
////
//
//import Foundation
//import Alamofire
//
//class HomeViewModel {
//    private let promotionRepository: MasterPromotionMenuCategoryItem1EndpointProtocol = MasterPromotionRepository.shared
//    private let specialCategoryId: Int
//    
//    //MARK: - Paging Variable
//    private var page = 1
//    private var totalPage = 1
//    private var isLoadNextPage = false
//    
//    //MARK: - Data Variable
//    let gamesData: Observable<[GamesResponsesResult?]> = Observable(nil)
//    let isLoading: Observable<Bool> = Observable(false)
//    let errorMessage: Observable<String?> = Observable(nil)
//    
//    init(
//        specialCategoryId: Int
//    ) {
//        self.specialCategoryId = specialCategoryId
//    }
//}
//class MasterGamesRepository: ApiProvider {
//    init() {
//    }
//    static let shared = MasterGamesRepository()
//}
//
//protocol HomeMasterGamesEndpointProtocol {
//    func getGames(pageSize: Int, completion: @escaping (Result<GamesResponses, Error>) -> Void)
//}
//extension MasterGamesRepository: HomeMasterGamesEndpointProtocol {
//    func getAll(categoryGroup: String?, page: Int, size: Int, id: Int?, completion: @escaping (Response<BaseResponse<[MenuCategory]>>) -> Void) {
//        let endpoint = MasterPromotionMenuCategoryItem1Endpoint.getAll(categoryGroup: categoryGroup, page: page, size: size, id: id)
//        loadRequest(endpoint: endpoint) { result in
//            completion(result)
//        }
//    }
//}
//enum MasterGamesHomeEndpoint {
//    
//    private static func getPath() -> String {
//        return "/games"
//    }
//    
//    static func getAll(categoryGroup: String?, page: Int, size: Int, id: Int?) -> NetworkEndpoint<BaseResponse<[MenuCategory]>> {
//        let path = getPath() + "special-category/item/\(page)/\(size)"
//        var queryParameter: [String:Any] = [:]
//        if categoryGroup != nil && id != nil {
//           queryParameter = ["category-group":categoryGroup ?? "", "special-category-id": id ?? 0]
//        } else if categoryGroup != nil {
//            queryParameter = ["category-group":categoryGroup ?? ""]
//        } else if id != nil {
//            queryParameter = ["special-category-id": id ?? 0]
//        }
//        return NetworkEndpoint(
//            path: path,
//            method: .get,
//            queryParameters: queryParameter)
//    }
//}
