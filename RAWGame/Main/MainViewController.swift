//
//  MainViewController.swift
//  RAWGame
//
//  Created by Ahmad Nur Alifulloh on 20/07/22.
//

import UIKit

class MainViewController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTabBarViews()
    }
    private func setupTabBarViews() {
        tabBar.tintColor = .black
        tabBar.unselectedItemTintColor = .lightGray
        
        tabBar.backgroundColor = UIColor(named: "SecondColor")
        
        let homeNavigationController = UINavigationController(rootViewController: HomeViewController())
        homeNavigationController.title = "Gamelog"
        homeNavigationController.tabBarItem.image = UIImage(named: "tabHomeUnselected")
        homeNavigationController.tabBarItem.selectedImage = UIImage(named: "tabHome")
        
        let aboutNavigationController = UINavigationController(rootViewController: AboutMeViewController())
        aboutNavigationController.title = "About"
        aboutNavigationController.tabBarItem.image = UIImage(named: "tabAboutUnselected")
        aboutNavigationController.tabBarItem.selectedImage = UIImage(named: "tabAbout")
        
        
        viewControllers = [
            homeNavigationController,
            aboutNavigationController
        ]
    }
    
}
